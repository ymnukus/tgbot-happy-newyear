package models

import (
	"time"
)

type Base struct {
	//ID        uuid.UUID `gorm:"type:uuid;primary_key;"`
	ID        uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	// DeletedAt gorm.DeletedAt `sql:"index"`
}

// BeforeCreate will set a UUID rather than numeric ID.
/*func (base *Base) BeforeCreate(db *gorm.DB) (err error) {
	if base.ID == uuid.Nil {
		base.ID = uuid.NewV4()
	}
	return nil
}*/
