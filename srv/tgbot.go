package srv

import (
	"log"
	"tgbot-newyear/db"
	"tgbot-newyear/db/models"
	"tgbot-newyear/libs"
	"tgbot-newyear/srv/handlers"
	"tgbot-newyear/tglibs"
	"tgbot-newyear/tgstructs"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var Bot *tgbotapi.BotAPI

func TGBot() {

	var err error
	Bot, err = tgbotapi.NewBotAPI(libs.Config.TBotApiToken)
	if err != nil {
		log.Panic(err)
	}

	if libs.Config.Env == "production" {
		Bot.Debug = false
	} else {
		Bot.Debug = true
	}

	log.Printf("Authorized on account %s", Bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := Bot.GetUpdatesChan(u)

	if err != nil {
		panic(err)
	}

	tglibs.SetBotContext(Bot)
	tglibs.Msgs = make(chan *tgstructs.Message)
	go tglibs.TGSendChannel(tglibs.Msgs)

	for update := range updates {

		processingMessage(&update)

	}
}

func processingMessage(update *tgbotapi.Update) {

	var err error
	var message *tgbotapi.MessageConfig

	chatID := tglibs.GetChatID(update)

	message, err = handlers.Handlers(update)

	if err != nil {
		tmp := tgbotapi.NewMessage(chatID, err.Error())
		tglibs.TGSendMessage(tmp)
	} else if message != nil {
		tglibs.TGSendMessage(*message)
	} else {

		tx := db.DB.Begin()
		defer func() {
			libs.EndTransaction(tx, err)
		}()

		var cats []models.CongratCats
		if res := tx.Find(&cats); res.RowsAffected == 0 {
			tmp := tgbotapi.NewMessage(chatID, "Ой! Что-то случилось...")
			tglibs.TGSendMessage(tmp)
		} else {

			tmp := tgbotapi.NewMessage(chatID, "Выберите действие")

			var buttons [][]tgbotapi.InlineKeyboardButton
			for i := range cats {
				b := tgbotapi.NewInlineKeyboardButtonData(cats[i].Display, cats[i].Name)
				row := tgbotapi.NewInlineKeyboardRow(b)
				buttons = append(buttons, row)
				if len(buttons) == 100 {
					var kb tgbotapi.InlineKeyboardMarkup
					kb.InlineKeyboard = buttons
					message.ReplyMarkup = kb
					tglibs.TGSendMessage(tmp)
					tmp = tgbotapi.NewMessage(chatID, "Продолжение")
					buttons = [][]tgbotapi.InlineKeyboardButton{}
				}
			}

			var kb tgbotapi.InlineKeyboardMarkup
			kb.InlineKeyboard = buttons

			tmp.ReplyMarkup = kb
			tglibs.TGSendMessage(tmp)
		}
	}

}
