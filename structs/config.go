package structs

type Config struct {
	TBotApiToken string
	Env          string

	MigrateDB           bool
	MigrateDBReferences bool
	DbName              string
	DbAddress           string
	DbPort              string
	DbLogin             string
	DbPassword          string
}
