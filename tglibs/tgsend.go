package tglibs

import (
	"reflect"
	"tgbot-newyear/tgstructs"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var bot *tgbotapi.BotAPI

var Msgs chan *tgstructs.Message

var deferredMessages = make(map[int64]chan *tgstructs.Message)
var lastMessageTimes = make(map[int64]int64)

func SetBotContext(context *tgbotapi.BotAPI) {
	bot = context
}

func TGSendMessage(msg tgbotapi.MessageConfig) {
	var message tgstructs.Message
	message.Message = &msg
	if _, ok := deferredMessages[msg.ChatID]; !ok {
		deferredMessages[msg.ChatID] = make(chan *tgstructs.Message, 1000)
	}
	deferredMessages[msg.ChatID] <- &message
}

func TGSendChannel(msgs chan *tgstructs.Message) {
	timer := time.NewTicker(time.Second / 30)
	for range timer.C {
		cases := []reflect.SelectCase{}
		for userId, ch := range deferredMessages {
			if userCanReceiveMessage(userId) && len(ch) > 0 {
				// Формирование case
				cs := reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch)}
				cases = append(cases, cs)
			}
		}

		if len(cases) > 0 {
			// Достаем одно сообщение из всех каналов
			_, value, ok := reflect.Select(cases)

			if ok {
				msg := value.Interface().(*tgstructs.Message)
				// Выполняем запрос к API
				if msg == nil {
					continue
				}
				if msg.Message != nil {
					if msg.Message.ChatID == 0 {
						continue
					}
					if msg.Message != nil {
						if _, err := bot.Send(msg.Message); err != nil {
							//log.Panic(err)
							continue
						}
						lastMessageTimes[msg.Message.ChatID] = time.Now().UnixNano()
					}
				}
			}
		}
	}
}

func userCanReceiveMessage(userId int64) bool {
	t, ok := lastMessageTimes[userId]

	return !ok || t+int64(time.Second) <= time.Now().UnixNano()
}
