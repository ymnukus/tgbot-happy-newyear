FROM golang:1.17.3-bullseye AS builder
WORKDIR /app/
COPY ./ ./
RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./main ./main.go
RUN ls -l

FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /
ENV TZ=Europe/Moscow
ENV ZONEINFO=/zoneinfo.zip
COPY --from=builder /app/main /
CMD ["./main"]