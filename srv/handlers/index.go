package handlers

import (
	"fmt"
	"math/rand"
	"tgbot-newyear/db"
	"tgbot-newyear/db/models"
	"tgbot-newyear/libs"
	"tgbot-newyear/tglibs"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func Handlers(event *tgbotapi.Update) (msg *tgbotapi.MessageConfig, err error) {

	if event.CallbackQuery != nil {

		tx := db.DB.Begin()
		defer func() {
			libs.EndTransaction(tx, err)
		}()

		var cats models.CongratCats
		if res := tx.Preload("CongratText").First(&cats, "name = ?", event.CallbackQuery.Data); res.RowsAffected == 0 {
			tmp := tgbotapi.NewMessage(tglibs.GetChatID(event), fmt.Sprintf("Что вы имели ввиду?"))
			msg = &tmp
			err = res.Error
			return
		}

		if len(cats.CongratText) == 0 {
			tmp := tgbotapi.NewMessage(tglibs.GetChatID(event), fmt.Sprintf("Извините, но я не знаю что Вам сказать..."))
			msg = &tmp
			return
		}

		num := rand.Int31n(int32(len(cats.CongratText)))

		tmp := tgbotapi.NewMessage(tglibs.GetChatID(event), cats.CongratText[num].Txt)
		tglibs.TGSendMessage(tmp)
	}

	return

}
