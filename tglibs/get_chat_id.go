package tglibs

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

func GetChatID(event *tgbotapi.Update) int64 {
	if event.Message != nil {
		return event.Message.Chat.ID
	} else if event.CallbackQuery != nil {
		return event.CallbackQuery.Message.Chat.ID
	}
	return 0
}
