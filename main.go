package main

import (
	"tgbot-newyear/db"
	"tgbot-newyear/libs"
	"tgbot-newyear/srv"
)

func main() {
	libs.LoadConfig()
	db.Connect()
	if !libs.Config.MigrateDB {
		libs.LoadConfig()
		srv.TGBot()
	}
}
