package models

type CongratText struct {
	Base
	// CongratCatsID uuid.UUID `gorm:"column:id_congrat_cat"`
	CongratCatsID uint `gorm:"column:id_congrat_cat"`
	CongratCats   CongratCats
	Txt           string `gorm:"column:txt;type:text"`
}
