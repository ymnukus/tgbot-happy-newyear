package db

import (
	"compress/gzip"
	"embed"
	"encoding/csv"
	"fmt"
	"io/fs"
	"log"
	"os"
	"time"

	"tgbot-newyear/db/models"
	"tgbot-newyear/libs"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

//go:embed cats.csv.gz
//go:embed congrats.csv.gz
var f embed.FS

var DB *gorm.DB

func Connect() {
	dsnMySQL := libs.Config.DbLogin + ":" + libs.Config.DbPassword + "@tcp(" + libs.Config.DbAddress + ":" + libs.Config.DbPort + ")/" + libs.Config.DbName + "?charset=utf8mb4&parseTime=True&loc=Local"

	var logLevel logger.LogLevel
	if libs.Config.Env == "production" {
		logLevel = logger.Silent
	} else {
		logLevel = logger.Info
	}

	var err error
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // Slow SQL threshold
			LogLevel:      logLevel,    // Log level
			Colorful:      false,       // Disable color
		},
	)
	newLogger.LogMode(logger.Info)
	DB, err = gorm.Open(mysql.Open(dsnMySQL), &gorm.Config{
		Logger:                                   newLogger,
		DisableForeignKeyConstraintWhenMigrating: true,
	})

	if err != nil {
		panic(err)
	}

	// Установим пул соединений
	dbSettings, err := DB.DB()
	if err != nil {
		panic(err)
	}
	dbSettings.SetMaxIdleConns(10)
	dbSettings.SetMaxOpenConns(50)
	dbSettings.SetConnMaxLifetime(time.Minute * 10)

	if libs.Config.MigrateDB {

		// Создание БД

		DB.AutoMigrate(
			&models.CongratCats{},
		)

		DB.AutoMigrate(
			&models.CongratText{},
		)

		migrateCats()
		migrateCongrats()
	}
}

func migrateCats() {
	var catsFile fs.File
	var err error

	catsFile, err = f.Open("cats.csv.gz")
	if err != nil {
		panic(err)
	}

	defer catsFile.Close()

	var gz *gzip.Reader
	gz, err = gzip.NewReader(catsFile)
	if err != nil {
		panic(err)
	}

	defer gz.Close()

	reader := csv.NewReader(gz)
	reader.Comma = ';'

	var record []string
	z := 0
	fmt.Println("Update categories")

	tx := DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	for {
		record, err = reader.Read()
		if err != nil {
			break
		}

		var cat models.CongratCats
		if res := tx.First(&cat, "name = ?", record[0]); res.RowsAffected == 0 {
			// Создаем
			cat.Name = record[0]
			cat.Display = record[1]
			if res := tx.Create(&cat); res.RowsAffected == 0 {
				panic(res.Error)
			}
		} else {
			// Обновляем
			cat.Display = record[1]
			if res := tx.Save(&cat); res.RowsAffected == 0 {
				panic(res.Error.Error())
			}
		}
	}

	fmt.Printf("Finished %d records\n", z)

	err = nil

}

func migrateCongrats() {
	var catsFile fs.File
	var err error

	catsFile, err = f.Open("congrats.csv.gz")
	if err != nil {
		panic(err)
	}

	defer catsFile.Close()

	var gz *gzip.Reader
	gz, err = gzip.NewReader(catsFile)
	if err != nil {
		panic(err)
	}

	defer gz.Close()

	reader := csv.NewReader(gz)
	reader.Comma = ';'

	var record []string
	fmt.Println("Update texts")

	tx := DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	// Очистим все поздравления. Нам они не нужны
	tx.Delete(&models.CongratText{}, "1 = 1")

	for {
		record, err = reader.Read()
		if err != nil {
			break
		}

		var cat models.CongratCats
		if res := tx.First(&cat, "name = ?", record[0]); res.RowsAffected == 0 {
			continue
		}

		// Добавим поздравление

		var text models.CongratText
		text.CongratCatsID = cat.ID
		text.Txt = record[1]

		if res := tx.Create(&text); res.RowsAffected == 0 {
			panic(res.Error)
		}

	}

	err = nil

}
