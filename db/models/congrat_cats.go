package models

type CongratCats struct {
	Base
	Name        string `gorm:"column:name;type:varchar(50);"`
	Display     string `gorm:"column:display;type:text"`
	CongratText []CongratText
}
