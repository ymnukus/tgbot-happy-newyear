package libs

import (
	"os"
	"tgbot-newyear/structs"
)

var Config structs.Config

func LoadConfig() {
	var exists bool
	Config.TBotApiToken = os.Getenv("TBOT_API_TOKEN")

	if Config.Env, exists = os.LookupEnv("ENV"); !exists {
		Config.Env = "development"
	}

	if Config.DbName, exists = os.LookupEnv("DB_NAME"); !exists {
		Config.DbName = "bot"
	}

	if Config.DbAddress, exists = os.LookupEnv("DB_ADDRESS"); !exists {
		Config.DbAddress = "bot"
	}

	if Config.DbLogin, exists = os.LookupEnv("DB_LOGIN"); !exists {
		Config.DbLogin = "bot"
	}

	if Config.DbPassword, exists = os.LookupEnv("DB_PASSWORD"); !exists {
		Config.DbPassword = "bot"
	}

	if Config.DbPort, exists = os.LookupEnv("DB_PORT"); !exists {
		Config.DbPort = "3306"
	}

	prepareArguments()
}

func prepareArguments() {
	args := os.Args[1:]
	for i := range args {
		switch args[i] {
		case "migrate":
			Config.MigrateDB = true
		}
	}
}
